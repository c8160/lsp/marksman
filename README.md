# marksman

LSP server for markdown files.

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## What is marksman?

Marksman is an LSP server for Markdown that provides completion, goto
definition, find references, diagnostics, etc. It also supports wiki-link-style
references that enable Zettelkasten-like note-taking. See more about Marksman's
features below.

See [the project website](https://github.com/artempyanykh/marksman)


## How to use this image

This image is meant to be a one-to-one replacement of a natively installed
`marksman`. Being an LSP server, you most likely won't interact with `marksman`
directly. Instead, your editor of choice will connect to it to provide the
spell and grammar checking to you.

Since most LSP extensions by default search for an appropriately named
executable rather than connecting via TCP/IP, you should provide a shell
wrapper `marksman` with the following contents:

```bash
#!/usr/bin/env bash

podman run --rm -i registry.gitlab.com/c8160/lsp/marksman:1.0.0 $@
```

Make sure to place it somewhere on your path and make it executable. If
everything works, you can now execute `marksman --help` and should see the help
text from `marksman`.

For the actual LSP integration, refer to the `marksman` documentation here:
https://github.com/artempyanykh/marksman#existing-editor-integrations3

Or refer to your text managers help/documentation for integrating LSP servers.



## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on GitLab:
https://gitlab.com/c8160/lsp/marksman/-/issues

