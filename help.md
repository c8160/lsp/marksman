marksman image, a markdown LSP server.

Packages the [marksman Github project][1].

Documentation:
For the underlying marksman project, see https://github.com/artempyanykh/marksman
For this container, see https://gitlab.com/c8160/lsp/marksman

Requirements:
None

Configuration:
None


[1]: https://github.com/artempyanykh/marksman
